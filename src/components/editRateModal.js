import React from 'react';
import './editRateModal.css';

const EditRateModal = ({ editableRate, handleInputChange, handleSaveRate }) => {
    return (
        <div className="modal">
            <div className="modal-content">
                <table className="edit-rate-table">
                    <tbody>
                        <tr>
                            <td>Product Code:</td>
                            <td>
                                <input
                                    type="text"
                                    value={editableRate.productCode}
                                    onChange={(e) => handleInputChange(e, 'productCode')}
                                />
                            </td>
                        </tr>
                        <tr>
                            <td>TDSP:</td>
                            <td>
                                <input
                                    type="text"
                                    value={editableRate.tdsp}
                                    onChange={(e) => handleInputChange(e, 'tdsp')}
                                />
                            </td>
                        </tr>
                        <tr>
                            <td>Term Months:</td>
                            <td>
                                <input
                                    type="number"
                                    value={editableRate.termMonths}
                                    onChange={(e) => handleInputChange(e, 'termMonths')}
                                />
                            </td>
                        </tr>
                        <tr>
                            <td>Commodity Rate:</td>
                            <td>
                                <input
                                    type="number"
                                    value={editableRate.commodityRate}
                                    onChange={(e) => handleInputChange(e, 'commodityRate')}
                                />
                            </td>
                        </tr>
                        <tr>
                            <td>Promo Code:</td>
                            <td>
                                <input
                                    type="text"
                                    value={editableRate.promoCode}
                                    onChange={(e) => handleInputChange(e, 'promoCode')}
                                />
                            </td>
                        </tr>
                        <tr>
                            <td>Offer Type:</td>
                            <td>
                                <input
                                    type="text"
                                    value={editableRate.offerType}
                                    onChange={(e) => handleInputChange(e, 'offerType')}
                                />
                            </td>
                        </tr>
                        <tr>
                            <td>Date:</td>
                            <td>
                                <input
                                    type="date"
                                    value={editableRate.startDate}
                                    onChange={(e) => handleInputChange(e, 'startDate')}
                                />
                            </td>
                        </tr>
                        <tr>
                            <td>Time:</td>
                            <td>
                                <input
                                    type="time"
                                    value={editableRate.startTime}
                                    onChange={(e) => handleInputChange(e, 'startTime')}
                                />
                            </td>
                        </tr>
                        <tr>
                            <td>Zone:</td>
                            <td>
                                <input
                                    type="text"
                                    value={editableRate.zone}
                                    onChange={(e) => handleInputChange(e, 'zone')}
                                />
                            </td>
                        </tr>
                    </tbody>
                </table>
                <button onClick={handleSaveRate}>Save</button>
            </div>
        </div>
    );
};

export default EditRateModal;