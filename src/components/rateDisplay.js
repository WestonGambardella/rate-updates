import React, { useState } from 'react';
import EditRateModal from './editRateModal';
import './rateDisplay.css';

const DisplayList = ({ rates, onUpdateRate, onDeleteRate }) => {
    const [editableRate, setEditableRate] = useState(null);

    const handleEditRate = (rate) => {
        setEditableRate(rate);
    };

    const handleInputChange = (e, key) => {
        const updatedRate = { ...editableRate, [key]: e.target.value };
        setEditableRate(updatedRate);
    };

    const handleSaveRate = () => {
        onUpdateRate(editableRate);
        setEditableRate(null);
    };

    return (
        <div className='rate-table-container'>
            <table className="rate-table">
                <thead>
                    <tr>
                        <th>Product Code</th>
                        <th>TDSP</th>
                        <th>Term Months</th>
                        <th>Commodity Rate</th>
                        <th>Promo Code</th>
                        <th>Offer Type</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Zone</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {rates.map((rate) => (
                        editableRate && editableRate.id === rate.id ? (
                            <EditRateModal
                                editableRate={editableRate}
                                handleInputChange={handleInputChange}
                                handleSaveRate={handleSaveRate}
                            />
                        ) : (
                            <tr key={rate.id}>
                                <td>{rate.productCode}</td>
                                <td>{rate.tdsp}</td>
                                <td>{rate.termMonths}</td>
                                <td>{rate.commodityRate}</td>
                                <td>{rate.promoCode}</td>
                                <td>{rate.offerType}</td>
                                <td>{rate.startDate}</td>
                                <td>{rate.startTime}</td>
                                <td>{rate.zone}</td>
                                <td>
                                    <button className='edit-button' onClick={() => handleEditRate(rate)}>Edit</button>
                                    <button className='delete-button' onClick={() => onDeleteRate(rate.id)}>X</button>
                                </td>
                            </tr>
                        )
                    ))}
                </tbody>
            </table>
        </div>
    );
};

export default DisplayList;