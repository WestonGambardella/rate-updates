import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { setAccessToken } from '../reducers/accessTokenReducer';
import { useNavigate } from "react-router-dom";

import './loginForm.css';

const REACT_APP_URL = process.env.REACT_APP_URL

const LoginForm = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const handleLogin = async (event) => {
    event.preventDefault();
    console.log(process.env.REACT_APP_URL)
    const url = `${REACT_APP_URL}/partner/signin`;
    const loginData = {
        "email": email,
        "password": password
      };
    const fetchConfig = {
      method: 'POST',
      body: JSON.stringify(loginData),
      headers: {
        "Content-Type": "application/json"
      }
    };
  
    try {
      const response = await fetch(url, fetchConfig);
  
      if (response.status === 200) { 
        const responseData = await response.json();
        const accessToken = responseData.access_token
        dispatch(setAccessToken(accessToken));
        navigate("/create-rates")
      } else if (response.status === 500) {
        alert("Wrong username or password");
      } else {
        console.error("Error:", response.statusText);
      }
    } catch (error) {
      console.error("Fetch error:", error);
    }
  };

  return (
    <div className='loginFormContainer'>
        <form onSubmit={handleLogin}>
          <div>
            <label htmlFor="username">Email:</label>
            <input
              type="text"
              id="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </div>
          <div>
            <label htmlFor="password">Password:</label>
            <input
              type="password"
              id="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </div>
          <button type="submit">Login</button>
        </form>
    </div>
  );
};

export default LoginForm;