import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { setAccessToken } from '../reducers/accessTokenReducer';
import DisplayList from './rateDisplay';
import ReturnedIdsList from './postedRateDisplay'
import CreateRateForm from './createRateForm';
import TexasRateForm from './texasRateForm';
import { useNavigate } from "react-router-dom";
import './createRates.css'

const CreateRate = () => {
    const [rates, setRates] = useState([]);
    const [postedRates, setPostedRateData] = useState([]);
    const [failedRates, setFailedRates] = useState([]);
    const [postedRateIds, setPostedRateIds] = useState([]);
    const [isTexasRateForm, setIsTexasRateForm] = useState(false);
    const token = useSelector(state => state.accessToken.accessToken);
    const navigate = useNavigate();
    const dispatch = useDispatch();

    const REACT_APP_URL = process.env.REACT_APP_URL

    const addItemToList = (itemName) => {
        setRates([...rates, itemName]);
    };

    const handleSignOut = () => {
        dispatch(setAccessToken(null));
        navigate("/login");
      };

    const onDeleteRate = (rateId) => {
        const updatedRates = rates.filter(rate => rate.id !== rateId);
        setRates(updatedRates);
    };

    const postRates = async () => {
        const fetchPromises = [];

        for (let i = 0; i < rates.length; i++) {
            const rate = rates[i];
            try {
                const postData = {
                    tdsp: rate.tdsp,
                    product_code: rate.productCode.toLowerCase(),
                    term_months: rate.termMonths,
                    billing_rates: { "commodity_rate": parseFloat(rate.commodityRate) },
                    average_rates: [],
                    effective_date: `${rate.startDate}T${rate.startTime}:00Z`,
                    documents_en: [{}],
                    documents_es: [],
                    external_meta_data: '{}',
                    verification_required: true,
                    promo_code: rate.promoCode,
                    offer_type: rate.offerType.toLowerCase(),
                    zone: rate.zone
                };
                const response = await fetch(`${REACT_APP_URL}/partner/create_product_rate`, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${token}`
                    },
                    body: JSON.stringify(postData)
                });
                if (!response.ok) {
                    if (response.status === 401) {
                        alert('Token expired');
                        navigate('/login')
                    } else {
                        throw new Error(`Failed to create rates`);
                    }
                }
                const data = await response.json();
                fetchPromises.push(data);
            } catch (error) {
                alert('Error publishing rate');
                fetchPromises.push(null);
            }
        }

        try {
            const results = await Promise.all(fetchPromises);
            const allFailed = results.every(result => result === null);
            if (!allFailed) {
                setPostedRateIds(results.filter(id => id));
                setPostedRateData(rates)
                setRates([]);
            }
        } catch (error) {
            console.error('Error publishing rates', error);
        }
    }

    const handleUpdateRate = (updatedRate) => {
        const index = rates.findIndex(rate => rate.id === updatedRate.id);
        if (index !== -1) {
            const updatedRates = [...rates];
            updatedRates[index] = updatedRate;
            setRates(updatedRates);
        }
    };

    const toggleForm = () => {
        setIsTexasRateForm(prevState => !prevState);
    };

    return (
        <>
        <button className='sign-out-button'onClick={handleSignOut}>Sign Out</button>
        <div className='container'>
            <div className="toggle-button-container">
                <button className='toggle-button'onClick={toggleForm}>
                    {isTexasRateForm ? 'Switch to Regular Rate Form' : 'Switch to Texas Rate Form'}
                </button>
            </div>
            {isTexasRateForm ? (
                <TexasRateForm onAdd={addItemToList} />
            ) : (
                <CreateRateForm onAdd={addItemToList} />
            )}
            <DisplayList rates={rates} onUpdateRate={handleUpdateRate} onDeleteRate={onDeleteRate}/>
            <div className='publish-button-container'>
                <button className='publish-rates-button' onClick={postRates}>Submit Rates</button>
            </div>
            <ReturnedIdsList postedRateIds={postedRateIds} postedRates={postedRates}/>
        </div>
        </>
    );
};

export default CreateRate;