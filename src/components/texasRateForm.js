import React, { useState } from 'react';


const TexasRateForm = ({ onAdd }) => {
  const [rateData, setRateData] = useState({
    id: 0,
    productCode: '',
    tdsp: '',
    commodityRate: '',
    termMonths: '',
    promoCode: '',
    offerType: '',
    startDate: '',
    startTime: '05:00',
    zone: ''
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setRateData({ ...rateData, [name]: value });
  };

  const handleAddRate = () => {
    if (
      !rateData.productCode ||
      !rateData.tdsp ||
      !rateData.commodityRate ||
      !rateData.termMonths ||
      !rateData.promoCode ||
      !rateData.offerType ||
      !rateData.startDate ||
      !rateData.startTime 
    ) {
      alert('Please fill in all required fields');
      return;
    }
    const newRateData = { ...rateData, id: rateData.id + 1 };
    onAdd(newRateData);
    setRateData({
      id: newRateData.id + 1,
      productCode: '',
      tdsp: '',
      commodityRate: '',
      termMonths: '',
      promoCode: '',
      offerType: '',
      startDate: '',
      startTime: '05:00',
      zone: ''
    });
  };

  return (
    <div className='rateInputContainer'>
        <h1>Texas Rate Form</h1>
      <input
        className='rateInput'
        type="text"
        name="productCode"
        value={rateData.productCode}
        onChange={handleChange}
        placeholder="Product Code"
      />
      <input
        className='rateInput'
        type="text"
        name="tdsp"
        value={rateData.tdsp}
        onChange={handleChange}
        placeholder="TDSP"
      />
      <input
        className='rateInput'
        type="number"
        name="commodityRate"
        value={rateData.commodityRate}
        onChange={handleChange}
        placeholder="Commodity Rate"
      />
      <input
        className='rateInput'
        type="number"
        name="termMonths"
        value={rateData.termMonths}
        onChange={handleChange}
        placeholder="Term Months"
      />
      <input
        className='rateInput'
        type="text"
        name="promoCode"
        value={rateData.promoCode}
        onChange={handleChange}
        placeholder="Promo Code"
      />
      <input
        className='rateInput'
        type="text"
        name="offerType"
        value={rateData.offerType}
        onChange={handleChange}
        placeholder="Offer Type"
      />
      <input
        className='rateInput'
        type="date"
        name="startDate"
        value={rateData.startDate}
        onChange={handleChange}
        placeholder="Start Date"
      />
      <input
        className='rateInput'
        type="time"
        name="startTime"
        value={rateData.startTime}
        onChange={handleChange}
        placeholder="Start Time"
      />
      <input
        className='rateInput'
        type="text"
        name="zone"
        value={rateData.zone}
        onChange={handleChange}
        placeholder="Zone"
      />
      <button className='addRateButton' onClick={handleAddRate}>Add Rate</button>
    </div>
  );
};

export default TexasRateForm;