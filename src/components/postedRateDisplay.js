import React from 'react';
import './postedRateDisplay.css';

const ReturnedIdsList = ({ postedRateIds, postedRates }) => {
    return (
        <div className='published-rates-container'>
            <h2 className='published-rate-header'>Published Rate Id's</h2>
            <table className='returned-rate-table'>
                <thead>
                    <tr>
                        <th>Returned Id</th>
                        <th>Product Code</th>
                        <th>TDSP</th>
                        <th>Commodity Rate</th>
                        <th>Zone</th>
                    </tr>
                </thead>
                <tbody>
                    {postedRateIds.map((id, index) => (
                        <tr key={index}>
                            <td>{id.rate_id}</td>
                            {postedRates[index] && (
                                <>
                                    <td>{postedRates[index].productCode}</td>
                                    <td>{postedRates[index].tdsp}</td>
                                    <td>{postedRates[index].commodityRate}</td>
                                    <td>{postedRates[index].zone || "NULL"}</td>
                                </>
                            )}
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
};

export default ReturnedIdsList;
