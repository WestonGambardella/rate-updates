import { BrowserRouter, Route, Routes } from 'react-router-dom';
import LoginForm from './components/loginForm';
import CreateRateForm from './components/createRates';

function App() {
  return (
    <BrowserRouter>
      <div id="root" style={{ 
        display: 'flex', 
        justifyContent: 'center', 
        alignItems: 'center',
        backgroundImage: 'linear-gradient(to bottom, #E4E4E4, #F0C964)'        }}>
        <Routes>
          <Route path="" element={<LoginForm/>}/>
          <Route path="/login" element={<LoginForm/>}/>
          <Route path="/create-rates" element={<CreateRateForm/>}/>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
