import { configureStore } from '@reduxjs/toolkit';
import accessTokenReducer from './reducers/accessTokenReducer';
import refreshTokenReducer from './reducers/refreshTokenReducer';


const store = configureStore({
  reducer: {
    accessToken: accessTokenReducer,
    refreshToken: refreshTokenReducer,
  },
});

export default store;