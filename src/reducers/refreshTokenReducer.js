import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  refreshToken: null,
};

const refreshTokenSlice = createSlice({
  name: 'refreshToken',
  initialState,
  reducers: {
    setRefreshToken(state, action) {
      state.refreshToken = action.payload;
    },
    clearRefreshToken(state) {
      state.refreshToken = null;
    },
  },
});

export const { setRefreshToken, clearRefreshToken } = refreshTokenSlice.actions;
export default refreshTokenSlice.reducer;