To Run Project: 
1. After clicking on gitlab project link:
    - fork and clone (git clone https://gitlab.com/WestonGambardella/rate-updates)
2. cd into project rate_updates
(create a virtual env, but not necessary)
3. If Node.js is not installed:
    - go to https://nodejs.org and install
4. run npm install
5. create .env file in project directory
6. create env var titled REACT_APP_URL and use url that aligns with db
7. run npm start

Workflow:
1. login using Admin credentials
2. File out rate form for non-texas rate updates
    - works for rates with and without zone
3. hit the "add rate" button
    -can edit and delete rates before publishing
4. When all rates are added, hit "submit rates" button
5. You'll see a bulleted list of returned Ids from newely created rates

![mainpage](images/mainpage_readme.png)